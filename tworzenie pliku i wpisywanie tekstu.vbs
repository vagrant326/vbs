' NewTextEC.vbs
' Sample VBScript to write to a file. With added error-correcting
' Author Guy Thomas http://computerperformance.co.uk/
' VBScript Write File
' Modded by Vagrant326
' ---------------------------------------------' 

Option Explicit
Dim objFSO, objFolder, objShell, objTextFile, objFile, filesys
Dim strDirectory, strFile, strText
strDirectory = "c:\textfile"
strFile = "\test.txt"
strText = "test.. {ENTER}test123 {ENTER}tested.."

' Create the File System Object
Set objFSO = CreateObject("Scripting.FileSystemObject")

' Check that the strDirectory folder exists
If objFSO.FolderExists(strDirectory) Then
 Set objFolder = objFSO.GetFolder(strDirectory)
Else
 Set objFolder = objFSO.CreateFolder(strDirectory)
 WScript.Echo "Just created " & strDirectory
End If

If objFSO.FileExists(strDirectory & strFile) Then
 Set objFolder = objFSO.GetFolder(strDirectory)
WScript.Echo "File already exists."
Else
 Set objFile = objFSO.CreateTextFile(strDirectory & strFile)
 Wscript.Echo "Just created " & strDirectory & strFile


set objFile = nothing
set objFolder = nothing

Set objShell = CreateObject("WScript.Shell")
 objShell.run ("Explorer" &" " & strDirectory & "\" )
Wscript.Sleep 1000
objShell.AppActivate "textfile"
objShell.SendKeys "{UP}"
objShell.SendKeys "{ENTER}"

Wscript.Sleep 2000

objShell.SendKeys "test.." 
objShell.SendKeys "{ENTER}"
objShell.SendKeys "test123" 
objShell.SendKeys "{ENTER}" 
objShell.SendKeys "tested.." 
objShell.SendKeys "{ENTER}" 
objShell.SendKeys "^s"
Wscript.Sleep 100
objShell.SendKeys "%{F4}" 
End If

WScript.Quit

' End of VBScript to write to a file with error-correcting Code